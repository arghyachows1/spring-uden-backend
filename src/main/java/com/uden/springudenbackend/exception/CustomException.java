package com.uden.springudenbackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
public class CustomException extends Exception{
	
	 public CustomException() { super(); }
	  public CustomException(String message) { super(message); }
	  public CustomException(String message, Throwable cause) { super(message, cause); }
	  public CustomException(Throwable cause) { super(cause); }
}

