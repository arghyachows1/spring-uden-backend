package com.uden.springudenbackend.model;

public class DataResponse<T> {
	private  T result;
	private String responseMessage;

	public DataResponse(T result){
		this.result = result;
	}
	
	public DataResponse(String responseMessage){
		this.responseMessage = responseMessage;
	}
	
	public DataResponse(T result,String responseMessage ) {
		this.result = result;
		this.responseMessage = responseMessage;
	}

	public T getResult() {
		return result;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

}

