package com.uden.springudenbackend.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "user")
public class User {
	private String id;
	private String username;
	private String role;
	private String mailId;
	private String password;
}
