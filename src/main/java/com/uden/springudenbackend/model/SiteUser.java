package com.uden.springudenbackend.model;

import java.lang.String;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;

@Document(collection = "Siteuser")
@Data
@Builder
public class SiteUser {

    @Id
    private String id;
    private String userType;
    private String username;
    private String email;
    private String password;
    private String captchaValidation;
 }