package com.uden.springudenbackend.service;

import java.util.List;
import com.uden.springudenbackend.model.SiteUser;


public interface SiteUserService {
	
	public List<SiteUser> getSiteUsers();
	
}
