package com.uden.springudenbackend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.uden.springudenbackend.model.SiteUser;
import com.uden.springudenbackend.repository.SiteUserRepository;
import java.util.List;

@Service
public class SiteUserServiceImpl implements SiteUserService {

	@Autowired
	SiteUserRepository siteUserRepository;

	@Override
	public List<SiteUser> getSiteUsers() {
		Iterable<SiteUser> userData = siteUserRepository.findAll();
		return (List<SiteUser>) userData;
	}

}
