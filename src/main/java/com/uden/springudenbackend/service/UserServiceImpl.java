package com.uden.springudenbackend.service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCrypt;

import com.uden.springudenbackend.model.User;
import com.uden.springudenbackend.model.DataResponse;
import com.uden.springudenbackend.exception.CustomException;
import com.uden.springudenbackend.repository.UserRepository;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
	@Autowired
	UserRepository userRepository;


	@Override
	public DataResponse<User> createNewUser(User user) throws CustomException {
		try {
			LOGGER.info("Started - Creating the User");
			if (user.getMailId() == null && user.getPassword() == null) {
				String responseMessage = "{ \"message\":\"UserName or Password is Empty\"}";
				return new DataResponse<>(responseMessage);
			}
		//	Base64.Encoder encoder = Base64.getEncoder();
			String password = hashPassword(user.getPassword());
			LOGGER.info("Encoded string: " + password);


			User users = userRepository.findByMailId(user.getMailId());

			if (users != null) {
				String responseMessage = "{ \"message\":\"UserName or MailId Already Exists\"}";
				return new DataResponse<>(responseMessage);
			} else {
				user.setPassword(password);
				User userResult = userRepository.save(user);
				String responseMessage = "{ \"message\":\"User Created Successfully\"}";
				LOGGER.info("Success - Creating the user");
				return new DataResponse<>(userResult, responseMessage);
			}

		} catch (Exception e) {
			LOGGER.info("Exception - Creating the user");
			e.printStackTrace();
			throw new CustomException("Error - Creating User");

		}
	}
	
	public static String hashPassword(String password_plaintext){
		String salt = BCrypt.gensalt(12);
		String hashed_password = BCrypt.hashpw(password_plaintext,salt);

		return (hashed_password);
	} 

	@Override
	public DataResponse<User> validateUser(User user) throws CustomException {
		try {
			LOGGER.info("Started - Validating the User");
			if (user.getMailId() == null && user.getPassword() == null) {
				String responseMessage = "{ \"message\":\"UserName or Password is Empty\"}";
				return new DataResponse<>(responseMessage);
			}
			User mailIdResult = userRepository.findByMailId(user.getMailId());
			if (mailIdResult == null) {
				String responseMessage = "{ \"message\":\"Incorrect MailId\"}";
				return new DataResponse<>(responseMessage);
			}
			if (mailIdResult != null) {
				Base64.Decoder decoder = Base64.getDecoder();
				String password = new String(decoder.decode(mailIdResult.getPassword()));
				LOGGER.info("Decoded string: " + password);

				if (!password.equals(user.getPassword())) {
					String responseMessage = "{ \"message\":\"Incorrect Password\"}";
					return new DataResponse<>(responseMessage);
				}

				String responseMessage = "{ \"message\":\"Success\"}";
				LOGGER.info("Success - Validating the User");
				return new DataResponse<>(mailIdResult, responseMessage);
			}
		} catch (Exception e) {
			LOGGER.info("Exception occured - Validating user");
			e.printStackTrace();
			throw new CustomException("Exception - Validating User");
		}

		throw new CustomException("Error - Validating User");
	}

}
