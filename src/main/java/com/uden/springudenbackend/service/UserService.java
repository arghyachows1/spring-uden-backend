package com.uden.springudenbackend.service;

import com.uden.springudenbackend.exception.CustomException;
import com.uden.springudenbackend.model.DataResponse;
import com.uden.springudenbackend.model.User;

public interface UserService {
	public DataResponse<User> createNewUser(User user)throws CustomException;

	public DataResponse<User> validateUser(User user)throws CustomException;


}
