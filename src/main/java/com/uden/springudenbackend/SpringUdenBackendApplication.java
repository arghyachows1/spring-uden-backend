package com.uden.springudenbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringUdenBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringUdenBackendApplication.class, args);
	}

}
