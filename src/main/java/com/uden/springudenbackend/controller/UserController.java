package com.uden.springudenbackend.controller;

import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uden.springudenbackend.exception.CustomException;
import com.uden.springudenbackend.model.DataResponse;
import com.uden.springudenbackend.model.User;
import com.uden.springudenbackend.service.UserServiceImpl;


@RestController
@RequestMapping(value="/User")
@CrossOrigin(origins="*")
public class UserController {

	@Autowired
	UserServiceImpl userServiceImpl;
	
	
	@RequestMapping(value = "/createUser" , method = POST)
	public DataResponse<User> createNewUser(@RequestBody User user)throws CustomException
	{
		return userServiceImpl.createNewUser(user);
	}
	
	@RequestMapping(value = "/ValidateUser" , method = POST)
	public DataResponse<User> validateUser(@RequestBody User user)throws CustomException
	{
		return userServiceImpl.validateUser(user);
	}
	
}
