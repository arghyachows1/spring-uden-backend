package com.uden.springudenbackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import com.uden.springudenbackend.model.SiteUser;
import com.uden.springudenbackend.service.SiteUserService;


@RestController
@CrossOrigin
@RequestMapping("/siteusers")
public class SiteUserController {

	@Autowired
	SiteUserService siteUserService;
	
	@GetMapping(value = "/getAll")
	public List<SiteUser> getSiteUsers() {
		return siteUserService.getSiteUsers();
    }
}