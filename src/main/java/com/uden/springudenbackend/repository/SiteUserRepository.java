package com.uden.springudenbackend.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.uden.springudenbackend.model.SiteUser;

@Repository
@Transactional
public interface SiteUserRepository extends MongoRepository<SiteUser, String> {

	public SiteUser findByUsername(String username);

	public SiteUser findById(ObjectId id);
	
}