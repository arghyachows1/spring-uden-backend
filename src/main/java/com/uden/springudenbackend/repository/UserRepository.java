package com.uden.springudenbackend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.uden.springudenbackend.model.User;

public interface UserRepository extends MongoRepository<User,String>{

	User findByMailIdAndPassword(String mailId, String password);

	User findByMailId(String mailId);

	User findByPassword(String password);
	
	User findOneById(String userId);

	User findOneByUsername(String username);

	Boolean existsByUsername(String username);

}
